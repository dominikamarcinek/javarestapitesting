package entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by gooornik on 20.09.2019.
 */
public class Cart {

    private String success;
    private int quantity;

    public String getSuccess() {
        return success;
    }

    public int getQuantity() {
        return quantity;
    }

}
