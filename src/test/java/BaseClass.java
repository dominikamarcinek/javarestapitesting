import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;

/**
 * Created by gooornik on 19.09.2019.
 */
public class BaseClass {

    protected static final String BASE_ENDPOINT = "https://api.github.com";

    CloseableHttpClient client;
    CloseableHttpResponse response;

    //legacy
    //HttpClient client = new DefaultHttpClient();

    @BeforeMethod
    public void setup(){
        client = HttpClientBuilder.create().build();
    }

    @AfterMethod
    public void closeResources() throws IOException {
        client.close();
        response.close();
    }

}
