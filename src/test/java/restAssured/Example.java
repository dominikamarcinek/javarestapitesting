package restAssured;

import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

import static org.hamcrest.Matchers.*;

/**
 * Created by gooornik on 20.09.2019.
 */
public class Example {

    @DataProvider
    public static Object[][] differentNames(){
        return new Object[][]{
                {"Jim"},
                {"Susan"}
        };
    }


    @Test(dataProvider = "differentNames")
    public void bookingReturnsCorrectCheckinWithDifferentNames(String name){

        given().
                baseUri("https://restful-booker.herokuapp.com/").
        when().
                get("/booking/1").
        then().
                statusCode(200).
                body("firstname", equalTo(name));
    }

    @Test(priority = 1)
    public void bookingPatchIsSuccessful(){
        given().
                baseUri("https://restful-booker.herokuapp.com/booking/1").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                cookie("token=d8b80eb7986b766").
                //header("Content-Type", "application/json").
                //header("Accept", "application/json").
                //header("Cookie", "token=abc123").
                formParam("firstname", "Lucky").
                formParam("lastname", "Luki");
        when().
                put().
        then().
                statusCode(200);
    }

    @Test(priority = 1)
    public void healthCheck() {
        given().
                baseUri("https://restful-booker.herokuapp.com/").
        when().
                get("/ping").
        then().
                statusCode(201);
    }

    @Test
    public void howManyBooks(){
        given().
                baseUri("https://restful-booker.herokuapp.com/").
        when().
                get("/booking").
        then().
                statusCode(200).
                body("bookingid", hasSize(10));
    }


}
