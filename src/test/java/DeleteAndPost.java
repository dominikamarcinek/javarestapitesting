import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static entities.User.LOGIN;

/**
 * Created by gooornik on 20.09.2019.
 */
public class DeleteAndPost extends BaseClass {

    @Test
    public void deleteIsSuccessful() throws IOException{

        HttpDelete request = new HttpDelete(BASE_ENDPOINT + "/repos/andrejss88/deleteme");

        //Authentication mechanisms: Basic, OAuth, OAuth2, JWT (JSON Web Toolkit)
        request.setHeader(HttpHeaders.AUTHORIZATION, "token ");

        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();

        Assert.assertEquals(actualStatusCode, 204);
    }

    @Test
    public void createRepoReturns201() throws IOException{

        //Create an HttpPost with a valid Endpoint
        HttpPost request = new HttpPost(BASE_ENDPOINT + "/user/repos");

        String auth = "email" + ":" + "password";
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
        String authHeader = "Basic " + new String(encodedAuth);

        //Set the Basic Auth Header - use https, encoding is not encryption!
        request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);

        //Define Json to Post and set as Entity
        String json = "{\"name\": \"deleteme\"}";
        request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));

        //Send
        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();
        Assert.assertEquals(actualStatusCode, 201);
    }

    //My example
    @Test
    public void addToCartReturns200() throws IOException {
        HttpPost request = new HttpPost("http://5.196.7.235/men/1-1-hummingbird-printed-t-shirt.html#/1-size-s/8-color-white");
        List<NameValuePair> params = new ArrayList<>();

        params.add(new BasicNameValuePair("id_product", "1"));
        params.add(new BasicNameValuePair("id_customization", "0"));
        params.add(new BasicNameValuePair("group[1]", "1"));
        params.add(new BasicNameValuePair("group[2]", "8"));
        params.add(new BasicNameValuePair("qty", "1"));
        params.add(new BasicNameValuePair("add", "1"));
        params.add(new BasicNameValuePair("action", "update"));

        request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
        response = client.execute(request);

        int actualStatusCode = response.getStatusLine().getStatusCode();
        String serverName = ResponseUtils.getHeader(response, "Server");

        //does not work - HTML response
        String jsonBody = EntityUtils.toString(response.getEntity());
        JSONObject jsonObject = new JSONObject(jsonBody);

        String successValue = (String) getValueFor(jsonObject, "success");
        int quantityValue = (int)getValueFor(jsonObject, "quantity");

        Assert.assertEquals(actualStatusCode, 200);
        Assert.assertEquals(serverName, "Apache/2.4.18 (Ubuntu)");
        Assert.assertEquals(successValue, "true");
        Assert.assertEquals(quantityValue, 4);
    }

    private Object getValueFor(JSONObject jsonObject, String key) {
        return jsonObject.get(key);
    }

}
