import org.apache.http.client.methods.HttpOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by gooornik on 20.09.2019.
 */
public class Options204 extends BaseClass{

    @Test
    public void optionsReturnsCorrectMethodList() throws IOException{

        String header = "Access-Control-Allow-Methods";
        String expectedReply = "GET, POST, PATCH, PUT, DELETE";

        HttpOptions request = new HttpOptions(BASE_ENDPOINT);
        response = client.execute(request);

        String actualValue = ResponseUtils.getHeader(response, header);
        Assert.assertEquals(actualValue, expectedReply);
    }

}
